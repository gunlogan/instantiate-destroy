﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _prefabs;
    public Transform _spawnPoint;

    Animator _animator;

    private void Start() {
        _animator = GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider other) {
        int index = Random.Range(0, _prefabs.Length);
        _animator.SetTrigger("ButtonPressed");
        Instantiate(_prefabs[index], _spawnPoint.position, _spawnPoint.rotation);
        Debug.Log("Spawning");
    }
}
