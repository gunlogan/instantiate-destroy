﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash : MonoBehaviour
{
    public GameObject _gameObject;

      // Destroy everything that enters the trigger
    void OnTriggerEnter(Collider _gameObject)
    {
        Destroy(_gameObject);
    }
}
