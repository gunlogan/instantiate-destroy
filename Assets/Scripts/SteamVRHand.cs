﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class SteamVRHand : MonoBehaviour
{
    public SteamVR_Input_Sources _hand;
    private List<Collider> _pickupColliders = new List<Collider>();
    private Collider _pickedupCollider;

    private Vector3 _lastFramePosition;

    private void Update() {
        if (SteamVR_Actions.default_GrabPinch.GetStateUp(_hand)) {
            Drop();
            Debug.Log("You have unparrented the object" + _hand);
        }

        if (SteamVR_Actions.default_GrabPinch.GetStateDown(_hand)) {
            PickUp();
        }

        if (SteamVR_Actions.default_GrabGrip.GetStateDown(_hand)) {
            Debug.Log("You are grabbing" + _hand);
        }
    }

    private void LateUpdate() {
        if (_pickedupCollider) {
            _lastFramePosition = _pickedupCollider.transform.position;
        }        
    }

    private void PickUp() {
        Collider[] nearbyObjects = Physics.OverlapSphere(transform.position, 0.05f);

        if (nearbyObjects.Length > 0) {
            Rigidbody rigidbody = nearbyObjects[0].GetComponent<Rigidbody>();

            if (rigidbody != null) {
                rigidbody.isKinematic = true;
                nearbyObjects[0].transform.SetParent(transform);
                _pickedupCollider = nearbyObjects[0];
            }
        }
    }

    private void Drop() {
        if (_pickedupCollider != null) {
            Rigidbody rigidbody = _pickedupCollider.GetComponent<Rigidbody>();
            rigidbody.isKinematic = false;
            _pickedupCollider.transform.SetParent(null);
            Vector3 velocityVector = _pickedupCollider.transform.position - _lastFramePosition;
            Debug.Log(velocityVector);
            rigidbody.velocity = velocityVector / Time.deltaTime;
            _pickedupCollider = null;
        }
    }
}
