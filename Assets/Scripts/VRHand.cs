﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class VRHand : MonoBehaviour
{
    public enum HandType {

        Left, 
        Right
    }

    public HandType _handType;

    private XRNode _handNode;
    private string _triggerButton;

    protected void Update() 
    {
        switch (_handType) {
            case HandType.Left:
                _handNode = XRNode.LeftHand;
                _triggerButton = "VRLeftTrigger";
                break;
            case HandType.Right:
                _handNode = XRNode.RightHand;
                _triggerButton = "VRRightTrigger";
                break;
            default:
                break;
        }

        transform.position = InputTracking.GetLocalPosition(_handNode);
        transform.rotation = InputTracking.GetLocalRotation(_handNode);

        if (Input.GetAxis(_triggerButton) > 0.1f) {
            Debug.Log(_handType + "hand pressed the trigger");

        }
    }
}
